//
//  ViewController.swift
//  ProductHunter
//
//  Created by Денис Шаклеин on 19/12/2016.
//  Copyright © 2016 desage. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    let cellId = "cellId"
    
    var categoriesData = [CellsData(), CellsData(), CellsData(), CellsData()]
    var newPostsDict = [String: Any]()
    var refresh = UIRefreshControl()
    var timer: Timer!
    var categories = ["tech", "games", "podcasts", "books",]
    var curCategoryId = 0
    var favoriteCategoryId = 0
    
    func fetchData() {
        
        let urlString = URL(string: "https://api.producthunt.com/v1/posts/all?search%5Bcategory%5D=\(self.categories[curCategoryId])&access_token=591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff")
        URLSession.shared.dataTask(with: urlString!, completionHandler: { (data, response, error) -> Void in
            
            do {
                
                let json = try(JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary)
                self.categoriesData[self.curCategoryId] = CellsData()
                let posts = json?["posts"]! as? [NSDictionary]
                for post in posts! {
                    let category = CategoryCell()
                    category.title = post["name"] as? String
                    category.tagline = post["tagline"] as? String
                    category.imgName = (post["thumbnail"] as! NSDictionary)["image_url"] as? String
                    category.upvotes = post["votes_count"] as? Int
                    category.screenImage = (post["screenshot_url"] as! NSDictionary)["850px"] as? String
                    category.url = post["redirect_url"] as? String
                    self.categoriesData[self.curCategoryId].cellsData.append(category)
                }
                
                
                DispatchQueue.main.async(execute: {
                    self.tableView?.reloadData()
                })
                
            } catch let err {
                print(err)
            }
            
        }) .resume()
    }
    let imageCache = NSCache<NSString, UIImage>()
    
    func loadImageUsingUrlString(urlString: String) {
        
        let url = NSURL(string: urlString)
        
        
        if imageCache.object(forKey: urlString as NSString) != nil  {
            return
        }
        
        URLSession.shared.dataTask(with: url! as URL, completionHandler: { (data, respones, error) in
            
            if error != nil {
                print(error)
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                if let imageToCache = UIImage(data: data!) {
                    self.imageCache.setObject(imageToCache, forKey: urlString as NSString)
                } else {
                    self.imageCache.setObject(UIImage(named: "defaule_image.png")!, forKey: urlString as NSString)
                }
                self.tableView?.reloadData()
            })
            
        }).resume()
    }
    
    func fetchAllCategories() {
        let categoriesCount = self.categories.count
        for i in 0...categoriesCount  {
            curCategoryId = i % categoriesCount
            fetchData()
        }
    }
    
    func checkNewPostsInFavorite() {
        let urlString = URL(string: "https://api.producthunt.com/v1/posts/all?search%5Bcategory%5D=\(self.categories[favoriteCategoryId])&access_token=591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff")
        URLSession.shared.dataTask(with: urlString!, completionHandler: { (data, response, error) -> Void in
            do {
                
                let json = try(JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary)
                let posts = json?["posts"] as? [NSDictionary]
                
                var i = 0
                while (i < self.categoriesData[self.favoriteCategoryId].cellsData.count && (posts?[i]["name"] as? String) != self.categoriesData[self.favoriteCategoryId].cellsData[i].title) {
                    i += 1
                }
                
                DispatchQueue.main.async(execute: {
                    self.newPostsDict["count"] = i
                    if i > 0 {
                        self.newPostsDict["last_post_title"] = posts?[0]["name"] as? String
                        self.newPostsDict["last_post_tagline"] = posts?[0]["tagline"] as? String
                        
                    } else  {
                        self.newPostsDict["last_post_title"] = "nothing to add"
                        self.newPostsDict["last_post_tagline"] = "nothing"
                    }
                })

                
            } catch let err {
                print(err)
            }
            
        }).resume()
    }
    
    func refreshWithInterval() {
        refreshData()
        
    }
    
    func refreshData() {
        checkNewPostsInFavorite()
        print(self.newPostsDict["count"] ?? -1)
        let count = self.newPostsDict["count"] as? Int
        if count != nil {
            if count! > 0 {
                UserNotificationManager.shared.addNotificationTimeIntervalTrigger(posts: newPostsDict)
            }
//            else {
//                UserNotificationManager.shared.addNotificationTimeIntervalTrigger(posts: newPostsDict)
//            }
        }
        fetchData()
        refresh.endRefreshing()
        print("refreshed")
    }
    
    func tapOnNavBar() {
        let categoriesCount = self.categories.count
        navigationItem.title = self.categories[(curCategoryId + 1) % categoriesCount]
        curCategoryId = (curCategoryId + 1) % categoriesCount
        fetchData()
        
    }
    func onButtonClick(button: UIButton) {
        let categoriesCount = self.categories.count
        button.setTitle(self.categories[(curCategoryId + 1) % categoriesCount], for: UIControlState.normal)
        curCategoryId = (curCategoryId + 1) % categoriesCount
        fetchData()
        
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        refresh.addTarget(self, action: #selector(TableViewController.refreshData), for: UIControlEvents.valueChanged)
        refresh.addTarget(self, action: #selector(TableViewController.refreshWithInterval), for: UIControlEvents.valueChanged)
        
        timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(refreshWithInterval), userInfo: nil, repeats: true)
        tableView.addSubview(refresh)
        
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        button.titleLabel?.highlightedTextColor = .black
        button.titleLabel?.textColor = .green
        button.titleLabel?.font = UIFont(name: "AvenirNext-Heavy", size: 20)
        
        button.setTitle(self.categories[curCategoryId], for: UIControlState.normal)
        button.setTitleColor(.black, for: UIControlState.normal)
        button.addTarget(self, action: #selector(onButtonClick(button:)), for: UIControlEvents.touchUpInside)
        self.navigationItem.titleView = button
        self.tableView?.reloadData()
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! techTableViewCell
        cell.titleLabel.text = self.categoriesData[self.curCategoryId].cellsData[indexPath.row].title
        cell.descrLabel.text = self.categoriesData[self.curCategoryId].cellsData[indexPath.row].tagline

        cell.upvotesImage.image = UIImage(named: "upvoteImage")
        cell.upvotesLabel.text = String(self.categoriesData[self.curCategoryId].cellsData[indexPath.row].upvotes ?? 0)
        if let imageUrl = self.categoriesData[self.curCategoryId].cellsData[indexPath.row].imgName {
            loadImageUsingUrlString(urlString: imageUrl)
            cell.imgView.image = imageCache.object(forKey: imageUrl as NSString)
        }
        if let screenUrl = self.categoriesData[self.curCategoryId].cellsData[indexPath.row].screenImage {
            loadImageUsingUrlString(urlString: screenUrl)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoriesData[self.curCategoryId].cellsData.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailSegue" {
            let detailVC = segue.destination as? DetailViewController
            if let indexPath = tableView.indexPathForSelectedRow?.row {
                detailVC?.data = self.categoriesData[self.curCategoryId].cellsData[indexPath]
                detailVC?.imageCache = self.imageCache
            }
        }
    }

}

