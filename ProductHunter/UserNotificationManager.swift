//
//  UserNotificationManager.swift
//  ProductHunter
//
//  Created by Денис Шаклеин on 31/12/2016.
//  Copyright © 2016 desage. All rights reserved.
//

import UIKit
import UserNotifications

class UserNotificationManager {
    
    static let shared = UserNotificationManager()
    func registerNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
            if granted {
                print("success get roots")
            } else {
                print(error)
            }
        })
        
    }
    
    func addNotificationTimeIntervalTrigger(posts: Dictionary<String, Any>) {
        let content = UNMutableNotificationContent()
        content.title = "ProductHunter"
        
        if posts["count"] as! Int == 1 {
            content.subtitle = posts["last_post_title"] as! String
            content.body = posts["last_post_tagline"] as! String
        } else {
            content.subtitle = "Checkout new posts"
            content.body  = "There are \(posts["count"] as! Int) new posts"
            
        }
        
        content.badge = posts["count"] as? NSNumber
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
        let request = UNNotificationRequest(identifier: "timeInterval", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            
        })
    }
}
