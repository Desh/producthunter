//
//  WebViewController.swift
//  ProductHunter
//
//  Created by Денис Шаклеин on 30/12/2016.
//  Copyright © 2016 desage. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    var getUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(getUrl)
        let getRequest = URLRequest(url: URL(string: getUrl)!)
        webView.loadRequest(getRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}
