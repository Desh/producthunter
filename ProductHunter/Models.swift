//
//  Models.swift
//  ProductHunter
//
//  Created by Денис Шаклеин on 22/12/2016.
//  Copyright © 2016 desage. All rights reserved.
//

import UIKit

class CellsData {
    var cellsData = [CategoryCell]()
}

class CategoriesData {
    var categoriesDate = [CellsData(), CellsData(), CellsData(), CellsData()]
}

class CategoryCell {
    var title: String?
    var tagline: String?
    var imgName: String?
    var url: String?
    var upvotes: Int?
    var screenImage: String?
}


class UpvoteView: UIView {
    
    override func awakeFromNib() {
        
        self.layoutIfNeeded()
        layer.cornerRadius = self.frame.height / 2.0
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = UIColor.lightGray.cgColor
    }
}

class GetButton: UIButton {
    override func awakeFromNib() {
        self.layoutIfNeeded()
        layer.cornerRadius = self.frame.height / 5
        layer.masksToBounds = true
    }
}
