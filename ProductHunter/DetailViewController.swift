//
//  ItemViewController.swift
//  ProductHunter
//
//  Created by Денис Шаклеин on 29/12/2016.
//  Copyright © 2016 desage. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var taglineLabel: UITextView!
    
    
    
    @IBOutlet weak var screenImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var imageCache = NSCache<NSString, UIImage>()
    var data = CategoryCell()
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.white
        taglineLabel.text = data.tagline
        nameLabel.text = data.title
        screenImage.image = imageCache.object(forKey: (data.screenImage! as NSString))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWebView" {
            let webVC = segue.destination as? WebViewController
            webVC?.getUrl = data.url!
        }
    }
    

}
