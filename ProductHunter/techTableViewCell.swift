//
//  techTableViewCell.swift
//  ProductHunter
//
//  Created by Денис Шаклеин on 19/12/2016.
//  Copyright © 2016 desage. All rights reserved.
//

import UIKit

class techTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var upvotesLabel: UILabel!
    @IBOutlet weak var upvotesImage: UIImageView!
    @IBOutlet weak var descrLabel: UILabel!
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
